import React, { useState } from 'react';
import SmartFormEdit from '../../components/form/form-edit';

function FormCreatePage() {
    // will implement the state on redux.
    const singleChoiceQuestion = {
        id: 1,
        title: '',
        type: 'singleChoice',
        description: '',
        answerChoices: [
            {
                id: 1,
                title: '',
                image: '',
            },
            {
                id: 2,
                title: '',
                image: '',
            },
            {
                id: 3,
                title: '',
                image: '',
            },
            {
                id: 4,
                title: '',
                image: '',
            },
        ],
        correctAnswer: [],
        explain: '',
        expireTime: 0,
    };
    const [form, setForm] = useState({
        id: 1,
        title: 'Hello',
        description: '',
        banner: '',
        questions: [],
        explain: '',
        expireTime: '',
    });

    const setExplainQuestionCallback = (explainText) => {
        setForm({ ...form, explain: explainText });
        console.log(form.explain);
    };
    const setTimerCallback = (timer) => {
        setForm({ ...form, expireTime: timer });
    };
    const onSaveQuestionCallBack = () => {
        // Push this form state to redux forms state
    };

    const onQuestionChangeCallback = (option, type, id) => {
        switch (option) {
            case 'add':
            // setForm({
            //     ...form,
            //     questions: [...form.questions, singleChoiceQuestion],
            // });
            // return;
        }
    };

    const onAnswerChangeCallback = () => {};

    return (
        <SmartFormEdit
            form={form}
            setForm={setForm}
            setExplainQuestionCallback={setExplainQuestionCallback}
            setTimerCallback={setTimerCallback}
            onSaveQuestionCallBack={onSaveQuestionCallBack}
            onQuestionChangeCallback={onQuestionChangeCallback}
            onAnswerChangeCallback={onAnswerChangeCallback}
        />
    );
}

export default FormCreatePage;
