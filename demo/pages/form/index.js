import { useState } from 'react';
import { Form } from 'antd';
import styled from '@xstyled/styled-components';
import EmptyForm from '../../components/form/empty-form';
import SmartForm from '../../components/form/presentation-smart-form';

const MainFormContainer = styled(Form)`
    width: 100%;
    min-height: 100vh;
    border-radius: 5px;
    border: 1px solid black;
    background: gray;
    overflow: hidden;
    display: flex;
`;

export default function MainForm() {
    const [forms, setForm] = useState([
        {
            title: 'De thi cuoi ki 2021',
            description: 'Luu y: Khong su dung tai lieu',
            banner: '',
            questions: [
                {
                    id: 1,
                    type: 'singleChoice',
                    title: '2 * 2 = ?',
                    description: '',
                    answerChoices: [
                        {
                            id: 1,
                            title: '4',
                            image: '',
                        },
                        {
                            id: 2,
                            title: '3',
                            image: '',
                        },
                        {
                            id: 3,
                            title: '1',
                            image: '',
                        },
                        {
                            id: 4,
                            title: '2',
                            image: '',
                        },
                    ],
                    correctAnswer: ['2'],
                    explain: '',
                    expireTime: 20,
                },
                {
                    id: 1,
                    title: '1 * 4 = ?',
                    type: 'singleChoice',
                    description: '',
                    answerChoices: [
                        {
                            id: 1,
                            title: '4',
                            image: '',
                            name: 'question',
                            type: 'checkbox',
                        },
                        {
                            id: 2,
                            title: '3',
                            image: '',
                            name: 'question',
                            type: 'checkbox',
                        },
                        {
                            id: 3,
                            title: '1',
                            image: '',
                            name: 'question',
                            type: 'checkbox',
                        },
                        {
                            id: 4,
                            title: '2',
                            image: '',
                            name: 'question',
                            type: 'checkbox',
                        },
                    ],
                    correctAnswer: ['1'],
                    explain:
                        'because: 2 + 2 = 1 + 1 + 1 + 1 = 3 + 1 = 8 - 4 = 4',
                    expireTime: 32,
                },
                {
                    id: 1,
                    title: 'Describe your pet',
                    type: 'essay',
                    description: '',
                    answer: '',
                    expireTime: 320,
                },
            ],
        },
    ]);

    const addForm = (newForm) => {
        setForm([...forms, newForm]);
    };

    const { title, description, questions } = forms[0];

    return (
        <MainFormContainer>
            {!forms && <EmptyForm addForm={addForm} />}

            {/* Demo - Previous view */}
            <SmartForm
                title={title}
                description={description}
                questions={questions}
            />
            {/* <PrimaryButton onClick={addQuestion}>Them cau hoi</PrimaryButton> */}
        </MainFormContainer>
    );
}
