import styled, { system } from '@xstyled/styled-components';
import { Row, Col } from 'antd';

export const Container = styled(Row)`
    display: flex;
    width: 100%;
    height: 100vh;

    ${'' /* Responsive */}
    ${'' /* Tablet */}
   
    @media (max-width: md) {
        flex-direction: column;
    }
`;

const StyledCol = styled(Col)`
    background-color: ${({ bg }) => bg || 'red'};

    @media();
    ${system}
`;

export default function TwoColumnLayout() {
    return (
        <Container>
            <StyledCol flex={{ _: 0, md: 3 }} bg='blue' />
            <StyledCol flex={2} bg='red' />
        </Container>
    );
}
