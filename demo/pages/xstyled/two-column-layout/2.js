import styled, { system } from '@xstyled/styled-components';
import { Row, Col, Typography } from 'antd';

const { Title } = Typography;

export const Container = styled(Row)`
    ${system}
`;

const StyledCol = styled(Col)`
    ${system}
`;

const StyledTitle = styled(Title)``;

export default function TwoColumnLayout() {
    return (
        <Container
            display='flex'
            flexDirection={{ _: 'column', md: 'row' }}
            w='100%'
            h='100vh'
            bg={'blue' || 'orange'}>
            <StyledCol flex={{ _: 0, md: 3 }} bg='blue'>
                <span>Hello there</span>
            </StyledCol>
            <StyledCol flex={2} bg='red'>
                <span>Hello team</span>
            </StyledCol>
        </Container>
    );
}
