import { Layout, Card } from 'antd';
import styled from '@xstyled/styled-components';
const { Header, Content, Footer } = Layout;

const Container = styled(Layout)`
    width: 100%;
    min-height: 100vh;
`;

const StyledContent = styled(Content)``;

const StyledCardList = styled.div`
    width: 100%;
    display: flex;
    flex-wrap: wrap;
`;

const StyledCardItem = styled(Card)`
    width: 20%;
    margin: 10px;

    ${'' /* Mobile */}
    @media (max-width: sm) {
        width: 100%;
    }
`;

function CardList() {
    return (
        <Container>
            <Header>Header</Header>
            <StyledContent>
                <StyledCardList>
                    <StyledCardItem title='Card title' bordered={false}>
                        <p>Card content</p>
                        <p>Card content</p>
                        <p>Card content</p>
                    </StyledCardItem>
                    <StyledCardItem title='Card title' bordered={false}>
                        <p>Card content</p>
                        <p>Card content</p>
                        <p>Card content</p>
                    </StyledCardItem>
                    <StyledCardItem title='Card title' bordered={false}>
                        <p>Card content</p>
                        <p>Card content</p>
                        <p>Card content</p>
                    </StyledCardItem>
                    <StyledCardItem title='Card title' bordered={false}>
                        <p>Card content</p>
                        <p>Card content</p>
                        <p>Card content</p>
                    </StyledCardItem>
                    <StyledCardItem title='Card title' bordered={false}>
                        <p>Card content</p>
                        <p>Card content</p>
                        <p>Card content</p>
                    </StyledCardItem>
                </StyledCardList>
            </StyledContent>
            <Footer>Footer</Footer>
        </Container>
    );
}

export default CardList;
