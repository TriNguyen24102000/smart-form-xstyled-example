import { Layout, Card } from 'antd';
import styled, { system } from '@xstyled/styled-components';
const { Header, Content, Footer } = Layout;

const Container = styled(Layout)`
    ${system}
`;

const StyledContent = styled(Content)``;

const StyledCardList = styled.div`
    ${system}
`;

const StyledCardItem = styled(Card)`
    ${system}
`;

function CardList() {
    return (
        <Container w={'100%'} h={'100vh'}>
            <Header>Header</Header>
            <StyledContent>
                <StyledCardList
                    w={'100%'}
                    display={'flex'}
                    flexWrap={{ _: 'nowrap', sm: 'wrap' }}
                    flexDirection={{ _: 'column', sm: 'row' }}>
                    <StyledCardItem
                        title='Card title'
                        bordered={false}
                        w={{ _: '100%', sm: '300px' }}
                        m={'10px'}>
                        <p>Card content</p>
                        <p>Card content</p>
                        <p>Card content</p>
                    </StyledCardItem>
                    <StyledCardItem
                        title='Card title'
                        bordered={false}
                        w={{ _: '100%', sm: '300px' }}
                        m={'10px'}>
                        <p>Card content</p>
                        <p>Card content</p>
                        <p>Card content</p>
                    </StyledCardItem>
                    <StyledCardItem
                        title='Card title'
                        bordered={false}
                        w={{ _: '100%', sm: '300px' }}
                        m={'10px'}>
                        <p>Card content</p>
                        <p>Card content</p>
                        <p>Card content</p>
                    </StyledCardItem>
                    <StyledCardItem
                        title='Card title'
                        bordered={false}
                        w={{ _: '100%', sm: '300px' }}
                        m={'10px'}>
                        <p>Card content</p>
                        <p>Card content</p>
                        <p>Card content</p>
                    </StyledCardItem>
                    <StyledCardItem
                        title='Card title'
                        w={{ _: '100%', sm: '300px' }}
                        bordered={false}
                        m={'10px'}>
                        <p>Card content</p>
                        <p>Card content</p>
                        <p>Card content</p>
                    </StyledCardItem>
                </StyledCardList>
            </StyledContent>
            <Footer>Footer</Footer>
        </Container>
    );
}

export default CardList;
