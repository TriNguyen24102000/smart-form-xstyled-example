import styled from '@xstyled/styled-components';
import MainForm from '../components/form/root-form';
import Center from '../components/layout/center';

export default function Home() {
    return (
        <Center>
            <MainForm />
        </Center>
    );
}
