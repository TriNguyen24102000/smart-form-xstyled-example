import '../styles/globals.css';
import 'antd/dist/antd.css';
import { defaultTheme, ThemeProvider } from '@xstyled/styled-components';
import GlobalStyle from '../components/global-styles';

const theme = {
    ...defaultTheme,
    colors: {
        primary: {
            background: 'black',
            textColor: 'white',
        },
        secondary: {
            background: 'orage',
            textColor: 'black',
        },
    },
};

function MyApp({ Component, pageProps }) {
    return (
        <ThemeProvider theme={theme}>
            <GlobalStyle />
            <Component {...pageProps} />
        </ThemeProvider>
    );
}

export default MyApp;
