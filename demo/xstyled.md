# How to use xStyled

### Add xStyled to project

```
    yarn add @xstyled/styled-components styled-components
```

### Import xStyled library to project wanna style.

```
    import styled from '@xstyled/styled-components';
    import { Row, Col } from 'antd';

    export const Container = styled(Row)`
        ...
    `;
```

## How to styled?

    - With original tag element like: a, h1, h2, div, span, ... we use `styled.tag`:
        Example: Suppose I wanna style 'div' tag with 100 percent width and 100vh height
        ```
            const Container = styled.div`
                width: 100%;
                height: 100vh;
            `;
        ```
        - How to use:
        ```
            export default function App() {
                return <Container></Container>
            }
        ```

    - With built-in component supported by Ant Design or our hand-made component, we use `styled(component)`:
        Example: Suppose I wanna style 'Button' component with `background` red and `color` blue
        ```
            import { Button } from 'antd';

            const StyleButton = styled(Button)`
                background: red;
                color: blue;
            `;
        ```
        - How to use: The same with above example

### There two approach to use with xStyled.

    - Use styled.* or styled(component) ( often use for common style) - component which just style once and affect all styled used it.
        - How to use: import xStyled library (@xstyled/styled-component) and use as normal.

    - Style directly to component -> Actually as pass props to xStyled and rendered, we can custom or use it design system ( Demo )
        - How to use: import {system} from '@xstyled/styled-components

### Responsive:

    - xStyled support us two 2 approachs to responsive.
        1. Use media query -> styled.* or styled(component)
        2. Use responsive unit on their component.
