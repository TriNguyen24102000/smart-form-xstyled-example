import { system } from '@xstyled/system';
import { Button } from 'antd';
import React from 'react';
import styled from 'styled-components';

const StyledRootButton = styled(Button)`
    ${system}
`;

function RootButton({
    bg,
    type,
    shape,
    isBlock,
    htmlType,
    icon,
    color,
    onClick,
    children,
}) {
    return (
        <StyledRootButton
            bg={bg || 'transparent'}
            color={color || 'blue'}
            type={type || 'primary'}
            htmlType={htmlType || 'button'}
            shape={shape}
            icon={icon}
            block={isBlock}
            onClick={onClick}>
            {children}
            <div></div>
        </StyledRootButton>
    );
}

export default RootButton;
