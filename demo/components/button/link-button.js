import React from 'react';
import RootButton from './root-button';

function LinkButton({ icon, isBlock, onClick, children }) {
    return (
        <RootButton type='link' isBlock={isBlock} icon={icon} onClick={onClick}>
            {children}
        </RootButton>
    );
}

export default LinkButton;
