import RootButton from './root-button';

function PrimaryButton({ icon, isBlock, onClick, children }) {
    return (
        <RootButton
            bg={({ theme }) => theme.colors.primary.background}
            color={({ theme }) => theme.colors.primary.color}
            type='primary'
            icon={icon}
            isBlock={isBlock}
            onClick={onClick}>
            {children}
        </RootButton>
    );
}

export default PrimaryButton;
