import React, { useState } from 'react';
import { Form, PageHeader } from 'antd';
import styled from '@xstyled/styled-components';
import SmartFormEdit from './form-edit';
import EmptyForm from './empty-form';
import SmartFormPresent from './presentation-smart-form';

const MainFormContainer = styled(Form)`
    width: 100%;
    min-height: 100vh;
    border-radius: 5px;
    border: 1px solid black;
    background: gray;
    overflow: hidden;
    display: flex;
`;

export default function MainForm() {
    const [forms, setForm] = useState([
        {
            title: 'De thi cuoi ki 2021',
            description: 'Luu y: Khong su dung tai lieu',
            banner: '',
            questions: [
                {
                    id: 1,
                    type: 'singleChoice',
                    title: '2 * 2 = ?',
                    description: '',
                    answerChoices: [
                        {
                            id: 1,
                            title: '4',
                            image: '',
                        },
                        {
                            id: 2,
                            title: '3',
                            image: '',
                        },
                        {
                            id: 3,
                            title: '1',
                            image: '',
                        },
                        {
                            id: 4,
                            title: '2',
                            image: '',
                        },
                    ],
                    correctAnswer: ['2'],
                    explain: '',
                    expireTime: 20,
                },
                {
                    id: 1,
                    title: '1 * 4 = ?',
                    type: 'singleChoice',
                    description: '',
                    answerChoices: [
                        {
                            id: 1,
                            title: '4',
                            image: '',
                        },
                        {
                            id: 2,
                            title: '3',
                            image: '',
                        },
                        {
                            id: 3,
                            title: '1',
                            image: '',
                        },
                        {
                            id: 4,
                            title: '2',
                            image: '',
                        },
                    ],
                    correctAnswer: ['1'],
                    explain:
                        'because: 2 + 2 = 1 + 1 + 1 + 1 = 3 + 1 = 8 - 4 = 4',
                    expireTime: 32,
                },
                {
                    id: 1,
                    title: 'Describe your pet',
                    type: 'essay',
                    description: '',
                    answer: '',
                    expireTime: 320,
                },
            ],
        },
    ]);

    const onQuestionChangeCallback = (option, id) => {
        switch (option) {
            case 'add':
                // do add question here
                return;
            case 'delete':
                // do remove question here
                return;
        }
    };

    const checkCorrectSubmitQuestion = (questionID, answerID) => {
        const questionMatch = forms.questions.find((q) => q.id === questionID);
        const answerMatch = questionMatch.answerChoices.find(
            (answer) => answer.id === answerID
        );
        console.log(questionMatch);
    };

    return (
        <MainFormContainer>
            {!forms && <EmptyForm />}

            {/* Demo - Previous view */}
            <SmartFormPresent
                title={title}
                description={description}
                questions={questions}
                checkCorrectSubmitQuestionCallback={checkCorrectSubmitQuestion}
            />
            {/* <PrimaryButton onClick={addQuestion}>Them cau hoi</PrimaryButton> */}
        </MainFormContainer>
    );
}
