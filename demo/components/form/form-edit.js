import React, { useState } from 'react';
import {
    Form,
    PageHeader,
    Typography,
    Input,
    Space,
    Select,
    Collapse,
} from 'antd';
import styled from '@xstyled/styled-components';
import StepBar from '../step-bar';
import PrimaryButton from '../button/primary-button';
import SingleChoiceQuestionEdit from '../question/question-edit/single-choice-question-edit';
import EssayQuestionEdit from '../question/question-edit/essay-question-edit';
import InvalidQuestion from '../question/invalid-question';
import RootQuestionEdit from '../question/question-edit/root-question-edit';

const { Text } = Typography;
const { Panel } = Collapse;
const { Option } = Select;

const StyledSmartFormEdit = styled(Form)`
    width: 100%;
    min-height: 100vh;
    border-radius: 5px;
    border: 1px solid black;
    background: white;
    overflow: hidden;
`;

const MainFormHeader = styled(PageHeader)``;

function InputGroup({ label, placeholder }) {
    return (
        <>
            <Text>{label}</Text>
            <Input placeholder={{ placeholder }} />
        </>
    );
}

function CreateBasicInfoStep({ callbacks }) {
    const [nextStep] = callbacks;
    return (
        <>
            <InputGroup label='Form name' placeholder='Ex: Ly thuyet do thi' />
            <InputGroup label='Topic' placeholder='Topic' />
            <InputGroup
                label='Short describe'
                placeholder='Ex: Chung ta se bat dau mon hoc nay nhe'
            />
            <PrimaryButton onClick={nextStep}>Next</PrimaryButton>
        </>
    );
}

function CreateQuestionStep({ type }) {
    // return <StyledQuestionContainer></StyledQuestionContainer>;
}

const generateQuestion = (
    type,
    id,
    title,
    explainQuestionCallback,
    setTimerCallback,
    onSaveQuestionCallBack,
    onAnswerChangeCallback,
    onQuestionChangeCallback
) => {
    switch (type) {
        case 'singleChoice':
            return (
                <SingleChoiceQuestionEdit
                    id={id}
                    title={title}
                    onQuestionChangeCallback={onQuestionChangeCallback}
                    onSaveQuestionCallBack={onSaveQuestionCallBack}
                    onAnswerChangeCallback={onAnswerChangeCallback}
                    setExplainQuestionCallback={explainQuestionCallback}
                    setTimerCallback={setTimerCallback}
                />
            );
        case 'essay':
            return (
                <EssayQuestionEdit
                    id={id}
                    title={title}
                    onQuestionChangeCallback={onQuestionChangeCallback}
                    onSaveQuestionCallBack={onSaveQuestionCallBack}
                    setExplainQuestionCallback={explainQuestionCallback}
                    setTimerCallback={setTimerCallback}
                />
            );
        default:
            return <InvalidQuestion />;
    }
};

const questionComponents = (questions) => {
    return questions.map((question) => {
        return generateQuestion();
    });
};

const generatePageByStep = (step, callback) => {
    switch (step) {
        case 0:
            return <CreateBasicInfoStep callbacks={callback} />;
        case 1:
            return <CreateQuestionStep callbacks={callback} />;
        case 2:
    }
};

function SmartFormEdit({
    form,
    setForm,
    setExplainQuestionCallback,
    setTimerCallback,
    onAnswerChangeCallback,
    onSaveQuestionCallBack,
    onQuestionChangeCallback,
}) {
    const [currentStep, setCurrentStep] = useState(0);
    const [questionType, setQuestionType] = useState('');

    const steps = [
        'Create Basic Info',
        'Create Form Content',
        'Share with other',
    ];

    const nextStep = () => {
        setCurrentStep(currentStep + 1);
    };

    const previousStep = () => {
        setCurrentStep(currentStep - 1);
    };

    const onChange = (value) => {
        setQuestionType(value);
    };

    return (
        <StyledSmartFormEdit>
            <MainFormHeader
                onBack={previousStep}
                title={steps[currentStep]}
                // extra={[<PrimaryButton icon={<PlusCircleOutlined />} />]}
            />
            <StepBar stepTitles={steps} currentStep={currentStep} />

            <Select
                showSearch
                style={{ width: 200 }}
                placeholder='Select a person'
                optionFilterProp='children'
                onChange={onChange}
                filterOption={(input, option) =>
                    option.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                }>
                <Option value='singleChoice'>Single Choice</Option>
                <Option value='essay'>Essay</Option>
            </Select>

            <PrimaryButton onClick={onQuestionChangeCallback('add')}>
                Them cau hoi
            </PrimaryButton>
            <div style={{ width: '600px' }}>
                {generateQuestion(
                    questionType,
                    form.id,
                    form.title,
                    setExplainQuestionCallback,
                    setTimerCallback,
                    onSaveQuestionCallBack,
                    onAnswerChangeCallback,
                    onQuestionChangeCallback
                )}
            </div>
        </StyledSmartFormEdit>
    );
}

export default SmartFormEdit;
