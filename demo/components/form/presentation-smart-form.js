import React, { useState } from 'react';
import { Form, PageHeader, Space, Typography } from 'antd';
import styled from '@xstyled/styled-components';
import InvalidQuestion from '../question/invalid-question';
import SingleChoiceQuestionPresent from '../question/question-present/single-choice-question-present';
import PrimaryButton from '../button/primary-button';
import EssayQuestionPresent from '../question/question-present/essay-question-present';

const { Text } = Typography;

const StyledSmartForm = styled(Form)`
    padding: 20px;
    width: 500px;
    min-height: 600px;
    border-radius: 5px;
    border: 1px solid black;
    background: white;
    overflow: hidden;
    margin: auto;
`;

// callback: like save the question, delete the question (dispatch to redux)
const generateQuestion = (question, callback) => {
    switch (question.type) {
        case 'singleChoice':
            return (
                <SingleChoiceQuestionPresent
                    {...question}
                    submitCallback={callback}
                />
            );
        case 'essay':
            return (
                <EssayQuestionPresent {...question} submitCallback={callback} />
            );
        default:
            return <InvalidQuestion />;
    }
};

const SmartFormHeader = styled(PageHeader)``;

const questionComponents = (questions, callback) => {
    return questions.map((question) => {
        return generateQuestion(question, callback);
    });
};

// Presentation form
export default function SmartFormPresent({
    questions,
    title,
    description,
    submitCallback,
    checkCorrectSubmitQuestionCallback,
}) {
    const [currentQuestion, setCurrentQuestion] = useState(0);

    const nextQuestion = (e) => {
        setCurrentQuestion(currentQuestion + 1);
    };

    const previousQuestion = (e) => {
        setCurrentQuestion(currentQuestion - 1);
    };

    return (
        <StyledSmartForm>
            <SmartFormHeader title={title} />
            <Text type={'warning'}>{description}</Text>

            {/* {questionComponents(questions, submitCallback)} */}
            {generateQuestion(
                questions[currentQuestion],
                checkCorrectSubmitQuestionCallback
            )}

            <Space style={{ display: 'flex', justifyContent: 'center' }}>
                {currentQuestion > 0 && (
                    <PrimaryButton onClick={previousQuestion}>
                        Previous
                    </PrimaryButton>
                )}
                {currentQuestion < questions.length - 1 && (
                    <PrimaryButton onClick={nextQuestion}>Next</PrimaryButton>
                )}
                {currentQuestion === questions.length - 1 && (
                    <PrimaryButton onClick={submitCallback}>Done</PrimaryButton>
                )}
            </Space>
        </StyledSmartForm>
    );
}
