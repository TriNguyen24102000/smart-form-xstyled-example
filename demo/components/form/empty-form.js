import { Typography } from 'antd';
import PrimaryButton from '../button/primary-button';
import Center from '../layout/center';
import SmartFormEdit from './form-edit';
import Link from 'next/link';

const { Text } = Typography;

function EmptyForm() {
    return (
        <Center>
            <Text>Seem like you dont have any form</Text>
            <br />
            <PrimaryButton>
                <Link href='/form/form-create'>Create one</Link>
            </PrimaryButton>
        </Center>
    );
}

export default EmptyForm;
