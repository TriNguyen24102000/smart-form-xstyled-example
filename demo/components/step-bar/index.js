import { Steps } from 'antd';
import styled, { system } from '@xstyled/styled-components';

const { Step } = Steps;

const StyledSteps = styled(Steps)`
    ${system}
`;

export default function StepBar({ stepTitles, currentStep }) {
    return (
        <StyledSteps
            w={{ _: '95%', md: '700px' }}
            size='small'
            current={currentStep}>
            {stepTitles.map((stepTitle, index) => {
                return <Step key={index} title={stepTitle} />;
            })}
        </StyledSteps>
    );
}
