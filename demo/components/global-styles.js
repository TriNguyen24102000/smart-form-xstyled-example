import { createGlobalStyle } from '@xstyled/styled-components';

const GlobalStyle = createGlobalStyle`
    * {
        padding: 0;
        margin: 0;
    }

    body {
        width: 100%;
        height: 100vh;
    }

`;

export default GlobalStyle;
