const StyledFlex = `
    display: flex;
    flex-direction: ${({ direction }) => direction || 'row'};
    width: 100%;
    height: 100%;
`;

export default function FlexLayout({ direction }) {
    return <StyledFlex direction={direction} />;
}
