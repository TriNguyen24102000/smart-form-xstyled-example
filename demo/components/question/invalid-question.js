import React from 'react';
import Center from '../layout/center';

function InvalidQuestion() {
    return (
        <Center>
            <div>Invalid Question</div>
        </Center>
    );
}

export default InvalidQuestion;
