import { Card, Input } from 'antd';
import { useRef, useState } from 'react';
import styled from 'styled-components';
import PrimaryButton from '../../button/primary-button';

const { TextArea } = Input;

const StyledSingleChoiceCard = styled(Card)`
    text-align: center;
    width: 100%;
`;

const StyledAnswerArea = styled(TextArea)`
    width: 100%;
    padding: 12px;
`;

function EssayQuestionPresent({
    id,
    title,
    answer,
    expireTime,
    submitCallback,
}) {
    const [userAnswerInput, setUserInput] = useState('');

    const handleSubmit = () => {
        console.log(userAnswerInput);
        // submitCallback(id, userAnswerInput);
    };

    const handleOnChange = (e) => {
        setUserInput(e.target.value);
    };

    return (
        <StyledSingleChoiceCard title={title} bordered={false}>
            <StyledAnswerArea rows={6} onChange={handleOnChange} />
            {expireTime && <div>{expireTime}s</div>}
            <PrimaryButton onClick={handleSubmit}>Submit Answer</PrimaryButton>
        </StyledSingleChoiceCard>
    );
}

export default EssayQuestionPresent;
