import { Card, Radio, Input, message } from 'antd';
import styled from 'styled-components';

const StyledSingleChoiceCard = styled(Card)`
    text-align: center;
    width: 100%;
`;

const StyledRadioGroup = styled(Radio.Group)``;

const StyledCardItem = styled(Radio.Button)`
    width: 200px;
    height: 100px;
    margin: 2px;
`;

function SingleChoiceQuestionPresent({
    id,
    title,
    answerChoices,
    expireTime,
    submitCallback,
}) {
    const handleOnChange = (e) => {
        if (!e.target.value) {
            message.error('You dont have submit yet');
            return;
        }
        console.log(id, e.target.value);
        // questionID, answerChoiceID
        submitCallback(id, e.target.value);
    };

    return (
        <StyledSingleChoiceCard title={title} bordered={false}>
            <StyledRadioGroup onChange={handleOnChange}>
                {answerChoices.map((answerChoice, index) => {
                    return (
                        <StyledCardItem key={index} value={answerChoice.id}>
                            {answerChoice.title}
                        </StyledCardItem>
                    );
                })}
            </StyledRadioGroup>
            {expireTime && <div>{expireTime}</div>}
        </StyledSingleChoiceCard>
    );
}

export default SingleChoiceQuestionPresent;
