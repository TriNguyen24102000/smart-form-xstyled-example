import React from 'react';
import styled from 'styled-components';
import RootQuestionEdit from './root-question-edit';
import { Input, Typography } from 'antd';

const { TextArea } = Input;
const { Title } = Typography;

const StyledEssayTextField = styled(TextArea)``;

function EssayQuestionEdit({
    id,
    title,
    explainQuestionCallback,
    setTimerCallback,
    onSaveQuestionCallBack,
}) {
    return (
        <RootQuestionEdit
            id={id}
            title={title}
            explainQuestionCallback={explainQuestionCallback}
            setTimerCallback={setTimerCallback}
            onSaveQuestionCallBack={onSaveQuestionCallBack}>
            <Title level={4}>Cau hoi</Title>
            <StyledEssayTextField rows={5} />
            <Title level={4}>Cau tra loi</Title>
            <StyledEssayTextField rows={5} />
        </RootQuestionEdit>
    );
}

export default EssayQuestionEdit;
