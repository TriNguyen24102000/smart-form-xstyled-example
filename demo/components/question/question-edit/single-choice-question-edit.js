import React, { useState } from 'react';
import { Input, Radio, Space } from 'antd';
import RootQuestionEdit from './root-question-edit';
import { DeleteOutlined } from '@ant-design/icons';
import PrimaryButton from '../../button/primary-button';
import { FileAddOutlined } from '@ant-design/icons';
import styled from '@xstyled/styled-components';
import LinkButton from '../../button/link-button';
import FlexLayout from '../../layout/flex-layout';

const { TextArea } = Input;

const StyledInput = styled(Input)`
    flex: 1;
`;

const QuestionMakerField = styled(TextArea)`
    width: 100%;
`;

export default function SingleChoiceQuestionEdit({
    id,
    title,
    explainQuestionCallback,
    setTimerCallback,
    onSaveQuestionCallBack,
    onAnswerChangeCallback,
    onQuestionChangeCallback,
}) {
    // const [singleChoiceQuestion, setSingleChoiceTemplate] = useState({
    //     id: 1,
    //     type: 'singleChoice',
    //     title: '2 * 2 = ?',
    //     description: '',
    //     answerChoices,
    //     correctAnswer: [],
    // });
    const [answerChoices, setAnswerChoices] = useState([
        {
            id: 1,
            title: '',
            image: '',
        },
        {
            id: 2,
            title: '',
            image: '',
        },
        {
            id: 3,
            title: '',
            image: '',
        },
        {
            id: 4,
            title: '',
            image: '',
        },
    ]);

    const handleOnChange = (index) => (e) => {
        // create new Answer choices.
        const newAnswerChoices = [...answerChoices];

        // change answer title of answer match with id.
        newAnswerChoices[index] = {
            ...newAnswerChoices[index],
            title: e.target.value,
        };

        setAnswerChoices(newAnswerChoices);
    };

    return (
        <RootQuestionEdit
            id={id}
            title={title}
            explainQuestionCallback={explainQuestionCallback}
            setTimerCallback={setTimerCallback}
            onSaveQuestionCallBack={onSaveQuestionCallBack}
            onQuestionChangeCallback={onQuestionChangeCallback}>
            <QuestionMakerField rows={5} />
            <div>
                <Radio.Group>
                    {answerChoices.map((answerChoice, index) => {
                        return (
                            <Radio value={answerChoice.id} key={index}>
                                <Space
                                    style={{
                                        display: 'flex',
                                        margin: '5px 0',
                                    }}>
                                    <StyledInput
                                        placeholder={
                                            'Dap an thu ' + (index + 1)
                                        }
                                        onChange={handleOnChange(index)}
                                    />
                                    <PrimaryButton
                                        icon={<DeleteOutlined />}
                                        onClick={onAnswerChangeCallback(
                                            'delete',
                                            answerChoice.id
                                        )}
                                    />
                                </Space>
                            </Radio>
                        );
                    })}
                </Radio.Group>
                <LinkButton
                    icon={<FileAddOutlined />}
                    onClick={onAnswerChangeCallback('add')}>
                    Them cau hoi
                </LinkButton>
            </div>
        </RootQuestionEdit>
    );
}
