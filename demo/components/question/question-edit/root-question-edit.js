import { Collapse, Switch, Input, Text, Space } from 'antd';
import PrimaryButton from '../../button/primary-button';
import { DeleteOutlined } from '@ant-design/icons';
import styled from '@xstyled/styled-components';

const { Panel } = Collapse;

const RootQuestionEditContent = styled(Panel)`
    width: 100%;
`;

const StyledQuestionContainer = styled(Collapse)``;

const StyledSpace = styled(Space)`
    width: 100%;
`;

export default function RootQuestionEdit({
    id,
    title,
    setExplainQuestionCallback,
    setTimerCallback,
    onSaveQuestionCallBack,
    onQuestionChangeCallback,
    children,
}) {
    return (
        <StyledQuestionContainer expandIconPosition='right'>
            <RootQuestionEditContent
                key={id}
                header={'Cau ' + id + ': ' + title}
                showArrow={true}
                extra={<DeleteOutlined onClick={onQuestionChangeCallback} />}>
                <StyledSpace direction='vertical'>
                    <StyledSpace direction='vertical'>{children}</StyledSpace>
                    <span>
                        <Switch checkedChildren='yes' unCheckedChildren='no' />
                        Question Explain?
                    </span>
                    <Input
                        onChange={(e) =>
                            setExplainQuestionCallback(e.target.value)
                        }
                    />
                    <span>
                        <Switch checkedChildren='yes' unCheckedChildren='no' />
                        Timer Set?
                    </span>
                    <Input onChange={setTimerCallback} />
                    <PrimaryButton isBlock onClick={onSaveQuestionCallBack}>
                        Save
                    </PrimaryButton>
                </StyledSpace>
            </RootQuestionEditContent>
        </StyledQuestionContainer>
    );
}
